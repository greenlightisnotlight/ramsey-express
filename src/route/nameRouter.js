import { Router } from "express"
export let nameRouter = Router()

nameRouter
.route("/")//localhost:8000/name
.get((req,res,next)=>{
    res.json("name get")
})
.post((req,res,next)=>{
    res.json("name post")
})
.patch((req,res,next)=>{
res.json("name update")
})
.delete((req,res,next)=>{
    res.json("name delete")
})